USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_encerra_atendimentos]    Script Date: 1/9/2024 11:17:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC proc_encerra_atendimentos;
ALTER PROCEDURE [dbo].[proc_encerra_atendimentos] AS
BEGIN

DECLARE @ID_PROC BIGINT
DECLARE @COUNT BIGINT

DECLARE @id INT 
DECLARE @protocolo VARCHAR(155)
DECLARE @status_id INT 
DECLARE @agendado_data VARCHAR(155) 


SET @ID_PROC=38
-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw SET total_meta= 1,total_pendente= 0 WHERE ID=@ID_PROC;
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
								VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
-------------------------------------------------------------------------------------------------------------------



DECLARE ListaAtendimentos CURSOR LOCAL STATIC FOR
	
	SELECT atendimentos_registros.id,atendimentos_registros.protocolo,atendimentos_registros.status_id,atendimentos_registros.agendado_data 
	FROM atendimentos_registros (nolock)
	LEFT JOIN atendimentos_senhas (nolock) ON atendimentos_senhas.atendimento_id = atendimentos_registros.id  
	LEFT JOIN vwatendimentos_tipos (nolock) ON vwatendimentos_tipos.id = atendimentos_registros.tipo_id  
	WHERE atendimentos_registros.status_id <> 3 
	AND vwatendimentos_tipos.tipo_id = 1 
	AND vwatendimentos_tipos.departamento_id NOT IN (8,6) 
	AND atendimentos_registros.agendado_data <= CONVERT(DATE,DATEADD(DAY,-1,GETDATE()))
	AND atendimentos_registros.ativo = 1


OPEN ListaAtendimentos
FETCH NEXT FROM ListaAtendimentos INTO @id,@protocolo,@status_id,@agendado_data

SET @COUNT = @@CURSOR_ROWS;

WHILE @@FETCH_STATUS = 0 
	BEGIN

		--Atualizando Tela 

		UPDATE 
		atendimentos_registros 
		SET status_id = 3,
		updated_at = GETDATE(),
		updated_por = 'TelaWeb - proc_encerra_atendimento' 
		WHERE atendimentos_registros.status_id <> 3 
		and id =@id 
		and protocolo = @protocolo

		INSERT INTO atendimentos_fluxo (created_at,updated_at,updated_por,atendimento_id,departamento_id,responsavel_id,status_id,descricao,resposta,lido,created_por) 
		VALUES (GETDATE(),GETDATE(),'TelaWeb - proc_encerra_atendimento',@id,1,0,3,'','Encerrado pelo Administrador Interno, por ter excedido o SLA para atendimento presencial',1,'TelaWeb - proc_encerra_atendimento')

		-- Próxima linha
		FETCH NEXT FROM ListaAtendimentos INTO @id,@protocolo,@status_id,@agendado_data
	END

CLOSE ListaAtendimentos
DEALLOCATE ListaAtendimentos

-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw SET data_exec = getdate(),[STATUS]=1,total_meta = 0,total_pendente = 0 WHERE ID=@ID_PROC;
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE(), registros_total = @COUNT where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
-------------------------------------------------------------------------------------------------------------------
		
END
-- EXEC proc_encerra_atendimentos;


