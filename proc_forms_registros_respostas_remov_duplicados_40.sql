USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_forms_registros_respostas_remov_duplicados_40]    Script Date: 1/9/2024 11:18:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC proc_forms_registros_respostas_remov_duplicados_40
ALTER PROCEDURE [dbo].[proc_forms_registros_respostas_remov_duplicados_40] AS
SET NOCOUNT ON;

DECLARE @ID_PROC BIGINT
DECLARE @COUNT BIGINT

SET @ID_PROC=40

-------------------------------------------------------------------------------------------------------------------
INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
								VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw SET total_meta= 1,total_pendente= 0 WHERE ID=@ID_PROC;
-------------------------------------------------------------------------------------------------------------------
DECLARE @supervisor VARCHAR(250)
DECLARE @periodo VARCHAR(250)
DECLARE @cliente VARCHAR(250)
DECLARE @qtd VARCHAR(250)
DECLARE @sql VARCHAR(MAX)
DECLARE @sqlI VARCHAR(MAX)

DECLARE Lista CURSOR LOCAL STATIC FOR
	select T1.resposta_texto AS periodo,T2.resposta_texto AS supervisor,T3.resposta_texto AS cliente,COUNT(T1.registro_id)QTD
	from      (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (318))T1 --Período
	left join (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (319))T2 on T1.registro_id=T2.registro_id --Supervisor
	left join (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (320))T3 on T1.registro_id=T3.registro_id --Cliente
	left join (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id in (421))T4 on T1.registro_id=T4.registro_id --Retorno
	GROUP BY T1.resposta_texto,T2.resposta_texto,T3.resposta_texto
	HAVING COUNT(T1.registro_id)>1
OPEN Lista
FETCH NEXT FROM  Lista INTO @periodo, @supervisor,@cliente,@qtd

SET @COUNT = @@CURSOR_ROWS;

WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT(@periodo+' - '+@supervisor+' - '+@cliente)
	--Deletando
	set @sql='delete forms_registros_respostas where form_id=40 and registro_id in 
						(select T1.registro_id from (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (318))T1
										left join (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (319))T2 on T1.registro_id=T2.registro_id 
										left join (select * from forms_registros_respostas (nolock) where form_id=40 and campo_id IN (320))T3 on T1.registro_id=T3.registro_id
										where T1.form_id=40 
										and T1.resposta_texto='''+ @periodo +'''
										and T2.resposta_texto='''+ @supervisor +'''
										and T3.resposta_texto='''+ @cliente +'''
										and T1.updated_at>=''2023-02-01 14:17:24.427''
										and T1.updated_por=''COLABORADOR TESTE - SERVAL LIMPEZA'')'
	EXEC(@sql)
	set @sqlI='delete forms_registros where form_id=40 and id not in (select registro_id 
										from forms_registros_respostas 
										where form_id=40)'
	EXEC(@sqlI)
FETCH NEXT FROM  Lista INTO @periodo, @supervisor,@cliente,@qtd
END
CLOSE Lista
DEALLOCATE Lista

UPDATE [DW].dbo.processos_dw SET data_exec = getdate(),STATUS=1,total_meta = 0,total_pendente = 0 WHERE ID=@ID_PROC;

-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE(), registros_total = @COUNT where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
-------------------------------------------------------------------------------------------------------------------



