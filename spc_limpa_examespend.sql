USE [CorporeRMV12]
GO
/****** Object:  StoredProcedure [dbo].[SPC_LIMPA_EXAMESPEND]    Script Date: 1/9/2024 11:26:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SPC_LIMPA_EXAMESPEND]
AS

BEGIN 

DECLARE @CODCOLIGADA INT
DECLARE @CHAPA VARCHAR(255)
DECLARE @CODPESSOA INT
DECLARE @CODEXAME VARCHAR(255)
DECLARE @CODPERFIL VARCHAR(255)
DECLARE @DATAPROXEXAME DATETIME
DECLARE @DATALCTOEXAME DATETIME
DECLARE @TIPOEXAME VARCHAR(255)
DECLARE @REALIZADO INT
DECLARE @DATARETORNO DATETIME
/*
DELETE FROM VEXAMESPEND WHERE TIPOEXAME = 'P'  AND REALIZADO = 0
*/
UPDATE VEXAMESPEND SET VEXAMESPEND.REALIZADO = 2
FROM VEXAMESPEND (NOLOCK) 
INNER JOIN PFUNC 
ON PFUNC.CODCOLIGADA = VEXAMESPEND.CODCOLIGADA 
AND PFUNC.CHAPA = VEXAMESPEND.CHAPA 
WHERE PFUNC.CODSITUACAO <> 'D' AND  VEXAMESPEND.TIPOEXAME = 'D' AND VEXAMESPEND.REALIZADO = 0 AND PFUNC.DATADEMISSAO IS NULL

UPDATE VEXAMESPEND  SET REALIZADO = 2
FROM (SELECT P.* FROM 
(SELECT * FROM VEXAMESPEND (NOLOCK) WHERE REALIZADO = 0 AND TIPOEXAME = 'A')P
INNER JOIN 
(SELECT * FROM VEXAMESPEND (NOLOCK) WHERE REALIZADO = 1 AND TIPOEXAME = 'A')R
ON R.CODCOLIGADA = P.CODCOLIGADA AND R.CHAPA = P.CHAPA AND R.TIPOEXAME = P.TIPOEXAME
AND R.CODEXAME = P.CODEXAME AND R.CODPERFIL = P.CODPERFIL
) query
WHERE VEXAMESPEND.CHAPA = QUERY.CHAPA 
AND VEXAMESPEND.CODCOLIGADA = QUERY.CODCOLIGADA 
AND VEXAMESPEND.TIPOEXAME = QUERY.TIPOEXAME
AND VEXAMESPEND.CODEXAME = QUERY.CODEXAME
AND VEXAMESPEND.CODPERFIL = QUERY.CODPERFIL
AND VEXAMESPEND.REALIZADO = 0


DECLARE ListaExames CURSOR FOR
	
	SELECT * FROM (SELECT 
					VEXAMESPEND.CODCOLIGADA,
					VEXAMESPEND.CHAPA,
					VEXAMESPEND.CODPESSOA,
					VEXAMESPEND.CODEXAME,
					VEXAMESPEND.CODPERFIL,
					VEXAMESPEND.DATAPROXEXAME,
					VEXAMESPEND.DATALCTOEXAME,
					VEXAMESPEND.TIPOEXAME,
					VEXAMESPEND.REALIZADO,
					((SELECT MAX(VEXAMESPRONT.DATAEXAME)
											  FROM VEXAMESPRONT (NOLOCK) 
										  INNER JOIN VEXAMES V1 (NOLOCK)
												ON V1.CODEXAME = VEXAMESPRONT.CODEXAME
												  AND V1.CODCOLIGADA = VEXAMESPRONT.CODCOLIGADA
												  AND V1.CODEXAME=VEXAMESPEND.CODEXAME
											 INNER JOIN VCONSULTASPRONT (NOLOCK)
												ON VCONSULTASPRONT.CODCOLIGADA = VEXAMESPRONT.CODCOLIGADA
											   AND VCONSULTASPRONT.IDCONSULTA = VEXAMESPRONT.IDCONSULTA
												WHERE VEXAMESPRONT.CODPESSOA = VEXAMESPEND.CODPESSOA
												AND VEXAMESPRONT.CODCOLIGADA = VEXAMESPEND.CODCOLIGADA
												AND VCONSULTASPRONT.CHAPA = VEXAMESPEND.CHAPA
												 AND VCONSULTASPRONT.TIPO = 1
												 AND VEXAMESPRONT.TIPOEXAME = VEXAMESPEND.TIPOEXAME
												 AND VEXAMESPRONT.DATAEXAME >= VEXAMESPEND.DATAPROXEXAME
										  )) AS 'DATARETORNO' 
					FROM VEXAMESPEND (NOLOCK)
					WHERE TIPOEXAME = 'R' AND REALIZADO = 0)T
					WHERE T.DATARETORNO IS NOT NULL


OPEN ListaExames
FETCH NEXT FROM ListaExames INTO @CODCOLIGADA,@CHAPA,@CODPESSOA,@CODEXAME,@CODPERFIL,@DATAPROXEXAME,
@DATALCTOEXAME, @TIPOEXAME,@REALIZADO,@DATARETORNO 

WHILE @@FETCH_STATUS = 0 
	BEGIN

		--Atualizando Retornos ao Trabalho 
		
		UPDATE 
		VEXAMESPEND 
		SET REALIZADO = 1,
		RECMODIFIEDBY = 'mestre',
		RECMODIFIEDON = GETDATE()
		WHERE CODCOLIGADA = @CODCOLIGADA AND CHAPA = @CHAPA AND CODPESSOA = @CODPESSOA
		AND CODEXAME = @CODEXAME AND CODPERFIL = @CODPERFIL AND TIPOEXAME = @TIPOEXAME
		AND DATAPROXEXAME = @DATAPROXEXAME

		-- Próxima linha
		FETCH NEXT FROM ListaExames INTO @CODCOLIGADA,@CHAPA,@CODPESSOA,@CODEXAME,@CODPERFIL,@DATAPROXEXAME,
		@DATALCTOEXAME, @TIPOEXAME,@REALIZADO,@DATARETORNO 
	END

CLOSE ListaExames
DEALLOCATE ListaExames

END


