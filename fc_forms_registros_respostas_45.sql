USE [TelaWeb]
GO
/****** Object:  UserDefinedFunction [dbo].[fc_forms_registros_respostas_45]    Script Date: 1/9/2024 11:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER function [dbo].[fc_forms_registros_respostas_45]()
returns @T table (
	id int,
	criado_at datetime, 
	atualizado_at datetime,
	mes int, --varchar(250),
	ano varchar(250),
	supervisor varchar(250),
	periodo varchar(250),
	funcionario varchar(250),
	localidade varchar(250),
	retornopasta varchar(250),
	qrcode varchar(250),
	indice varchar(250),
	data1 date, --varchar(250),
	data2 date, --varchar(250)
	indiceII varchar(250)
)
as

begin
	declare @ID int
	declare @criado_at datetime 
	declare @atualizado_at datetime 
	declare @ID_Campo int
	declare @Resposta varchar(250)

	declare @Empresa varchar(250)
	declare @EmpresaII varchar(250)
	declare @CodCliente varchar(250)
	declare @Funcionario varchar(250)
	declare @Funcionario_II varchar(250)
	declare @matricula varchar(250)
	declare @CodLocalidade varchar(250)
	declare @Lotacao varchar(250)
	declare @NomeLocalidade_II varchar(250)
	declare @Periodo_II varchar(250)
	declare @Supervisor_II varchar(250)
	declare @Periodo varchar(250)
	declare @Posto varchar(250)
	declare @Supervisor varchar(250)
	declare @RetornoPasta varchar(250)
	declare @Ano varchar(250)
	declare @Mes varchar(250)
	declare @Dia varchar(250)
	declare @Aux varchar(250)
	declare @AuxII varchar(250)
	declare @AuxIII varchar(250)
	
	declare @AnoII varchar(250)
	declare @MesII varchar(250)
	declare @DiaII varchar(250)
	
	declare @COUNT int
	declare @OCOR int
	declare @qrcode varchar(250)
   
	declare @SQL varchar(MAX)

	SET @COUNT = 0

	declare Lista45 cursor Local Static for	
		SELECT registro_id,created_at,updated_at,campo_id,resposta_texto 
		from forms_registros_respostas (nolock)
		where form_id = 45 AND campo_id in (404,405,406,407,408,421)
		order by registro_id desc,campo_id asc

		--SELECT ID,Registro_Criado_At,Registro_Atualizado_At,ID_Campo,Resposta 
		--from vwforms_registros_respostas 
		--where ID_Formulario = 45 AND ID_Campo in (404,405,406,407,408,421)
		--order by ID desc,ID_Campo asc


	OPEN Lista45
	FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- 404 Empresa
			-- 405 Supervisor
			-- 406 Funcionário
			-- 407 Lotação
			-- 408 Período
			-- 421 RetornoPasta	
			
			IF (@ID_Campo = 404) 
				BEGIN
					SET @Empresa = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 405) 
				BEGIN
					SET @Supervisor = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 406) 
				BEGIN
					SET @Funcionario = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 407) 
				BEGIN
					SET @Lotacao = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 408) 
				BEGIN
					SET @Periodo = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 421) 
				BEGIN
					SET @RetornoPasta = @Resposta
				END

			--(01 - Cliente - Localidade) Retirando apenas o Código da localidade e buscando o código do cliente
			SET @Aux = dbo.GetNumeric(RIGHT(@Lotacao,CHARINDEX('(',RIGHT(@Lotacao,9))+1))
			SELECT @CodCliente = cliente_id FROM [TelaWeb].[dbo].[vwpostos] (NOLOCK) WHERE id = @Aux
			SET @qrcode = ISNULL(@CodCliente,'0')  + '-' + ISNULL(@Aux,'0') 
			--(02 - Funcionario - Matricula) Retirando apenas a Matricula do Funcionario	
			SET @Aux = Right(replicate('0000000',7) + convert(VARCHAR,dbo.GetNumeric(RIGHT(@Funcionario,7))),7) 
			SET @AuxIII = LEFT(@Empresa,1) + '-' + @Aux
			SET @qrcode = @qrcode + '-' + ISNULL(@Aux,'0') 
			--(03 - Funcionario - Nome) Retirando apenas o nome do funcionario 
			SET @Aux = SUBSTRING(RTRIM(LTRIM(@Funcionario)),1,CHARINDEX(' (',@Funcionario)-1)
			SET @qrcode = @qrcode + '-' + ISNULL(@Aux,'0')  
			--(04 - Periodo)
			SET @Dia = SUBSTRING(@Periodo,1,2)
			SET @Mes = SUBSTRING(@Periodo,4,2)
			SET @Ano = SUBSTRING(@Periodo,7,4)
			SET @DiaII = SUBSTRING(@Periodo,14,2)
			SET @MesII = SUBSTRING(@Periodo,17,2)
			SET @AnoII = SUBSTRING(@Periodo,20,4)
			IF (@Ano = '2022') 
				SET @AuxIII = @AuxIII + '-' + @Ano + '/' +@Mes
			ELSE 
				SET @AuxIII = @AuxIII + '-' + @AnoII + '/' +@MesII + 'de' + @Dia + 'a' + @DiaII

			SET @Periodo = @Ano + '-' + @Mes + '-' + @Dia + 'a' + @AnoII + '-' + @MesII + '-' + @DiaII
			SET @Aux = @Periodo -- CONVERT(varchar,CAST(SUBSTRING(@Periodo,1,10)AS datetime),31) + 'a' + CONVERT(VARCHAR,CAST(SUBSTRING(@Periodo,14,20) AS DATETIME),31)
			SET @qrcode = @qrcode + '-' + ISNULL(@Aux,'0')  
			SET @qrcode = @qrcode + '-' + LEFT(@Empresa,1)
			IF LEN(@RetornoPasta)>0
			BEGIN
				SET @qrcode = @qrcode + '-' + @RetornoPasta
			END
			ELSE
			BEGIN
				SET @qrcode = @qrcode + '-PENDENTE'
			END

			

--select * from fc_forms_registros_respostas_45()   order by id desc where ano like '%-%'
			insert into @T 
				(id,qrcode,periodo,funcionario,localidade,supervisor,retornopasta,mes,ano,criado_at,atualizado_at,indice,data1,data2,indiceII)
			values 
				(@id,@qrcode,@Periodo,@Funcionario,@Lotacao,@Supervisor,@RetornoPasta,
				CASE WHEN @Ano='2023' AND @MesII>'01' THEN @MesII ELSE @Mes END,--@Mes,--
				CASE WHEN @Ano='2023' AND @MesII>'01' THEN @AnoII ELSE @Ano END,--@Ano,--
				@criado_at,@atualizado_at,CONVERT(VARCHAR,@ID) +'#'+@qrcode,@Ano +'-'+ @Mes +'-'+ @Dia,@AnoII +'-'+ @MesII +'-'+ @DiaII,@AuxIII)
	
			--insert into @T 
			--	(id,qrcode,periodo,funcionario,localidade,supervisor,retornopasta,mes,ano,criado_at,atualizado_at,indice)
			--values 
			--	(@id,@qrcode,@Periodo,@Funcionario,@Lotacao,@Supervisor,@RetornoPasta,
			--	@Mes,--@Mes,--
			--	@Ano,--@Ano,--
			--	@criado_at,@atualizado_at,CONVERT(VARCHAR,@ID) +'#'+@qrcode)

			FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@ID_Campo,@Resposta 
		END
	CLOSE Lista45
	DEALLOCATE Lista45
	
	return
end
--mes	ano	supervisor	periodo
--2-	0-11	JONATHAN MESQUITA (96)	0-11-2--20a10-11-22