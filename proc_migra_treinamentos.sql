USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_migra_treinamentos]    Script Date: 1/9/2024 11:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC proc_migra_treinamentos;
ALTER PROCEDURE [dbo].[proc_migra_treinamentos] AS
BEGIN
	SET NOCOUNT ON;
DECLARE @DATAATUALIZACAO DATETIME
DECLARE @ID_PROC BIGINT

DECLARE @COUNT BIGINT
DECLARE @CONT BIGINT

DECLARE @id INT 
DECLARE @created_at DATETIME
DECLARE @updated_at DATETIME
DECLARE @updated_por VARCHAR(250)
DECLARE @curso_id INT
DECLARE @nome VARCHAR(250)
DECLARE @instrutor_id INT
DECLARE @data DATETIME
DECLARE @obs VARCHAR(255)
DECLARE @anexo VARCHAR(255)
DECLARE @periodo_data1 DATE
DECLARE @periodo_data2 DATE 
DECLARE @created_por VARCHAR(255)
DECLARE @conteudo VARCHAR(MAX)

DECLARE @novo_id_turma INT 

DECLARE @curso_id_empresas INT
DECLARE @turma_id_empresas INT
DECLARE @coligada_id_empresas INT

DECLARE @curso_id_funcoes INT
DECLARE @turma_id_funcoes INT
DECLARE @funcao_id_funcoes VARCHAR(250)

DECLARE @curso_id_participantes INT
DECLARE @turma_id_participantes INT
DECLARE @matricula_participantes VARCHAR(250)
DECLARE @situacao_id_participantes INT
DECLARE @situacao_data_participantes datetime
DECLARE @nota_participantes int
DECLARE @anexo_participantes VARCHAR(250)





DECLARE ListaTurmas CURSOR LOCAL STATIC FOR
		SELECT id,
		created_at,
		updated_at,
		updated_por,
		curso_id,
		instrutor_id,
		nome,
		data,
		obs,
		anexo,
		periodo_data1,
		periodo_data2,
		created_por,
		conteudo
		from treinamentos_cursos_turmas (nolock)
		where  id  in (SELECT turma_id FROM vwtreinamentos_cursos_turmas
			where segregacao_id <> 3 and ead = 1 group by turma_id)

--PEGA OS REGISTROS ANTERIOR PARA MIGRAÇÃO E INICIA UM LOOP
OPEN ListaTurmas
FETCH NEXT FROM ListaTurmas INTO @id,@created_at,@updated_at,@updated_por,@curso_id,
@instrutor_id,@nome,@data,@obs,@anexo,@periodo_data1,@periodo_data2,@created_por,@conteudo

SET @COUNT = @@CURSOR_ROWS;

WHILE @@FETCH_STATUS = 0 
	BEGIN
		INSERT INTO treinamentos_cursos_turmas_matriculados
		(created_at,updated_at,updated_por,curso_id,instrutor_id,nome,data,obs,anexo,periodo_data1,periodo_data2,
		 created_por,conteudo,matricula_auto)
		 VALUES
		 (getdate(),getdate(),'ADMINISTRADOR INTERNO',@curso_id,
		  @instrutor_id,@nome,@data,@obs,@anexo,@periodo_data1,@periodo_data2,'ADMINISTRADOR INTERNO',@conteudo,0)


		SELECT @novo_id_turma = max(id) from treinamentos_cursos_turmas_matriculados (nolock) 
		where created_at >= DATEADD(HOUR, -1, GETDATE()) and created_por = 'ADMINISTRADOR INTERNO'
		
		--PROCURANDO AS EMPRESAS QUE POSSUEM ESSA TURMA, PARA MIGRÁ-LAS PARA A NOVA TABELA
		DECLARE ListaAtualizacaoEmpresas CURSOR LOCAL STATIC FOR
			
			select  
			treinamentos_cursos_turmas_empresas.curso_id,
			treinamentos_cursos_turmas_empresas.turma_id,
			treinamentos_cursos_turmas_empresas.coligada_id
			from treinamentos_cursos_turmas_empresas (nolock)
			left join treinamentos_cursos (nolock) on treinamentos_cursos.id = treinamentos_cursos_turmas_empresas.curso_id
			where treinamentos_cursos.ead = 1 and treinamentos_cursos.segregacao_id <> 3 and treinamentos_cursos_turmas_empresas.turma_id = @id
			group by
			treinamentos_cursos_turmas_empresas.curso_id,
			treinamentos_cursos_turmas_empresas.turma_id,
			treinamentos_cursos_turmas_empresas.coligada_id
			order by turma_id

		OPEN ListaAtualizacaoEmpresas
		FETCH NEXT FROM ListaAtualizacaoEmpresas INTO @curso_id_empresas,@turma_id_empresas,@coligada_id_empresas

		WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO treinamentos_cursos_turmas_matriculados_empresas
				(created_at,updated_at,updated_por,curso_id,turma_id,coligada_id,created_por)
				values
				(getdate(),getdate(),'ADMINISTRADOR INTERNO',@curso_id_empresas,@novo_id_turma,@coligada_id_empresas,'ADMINISTRADOR INTERNO')
				FETCH NEXT FROM ListaAtualizacaoEmpresas INTO @curso_id_empresas,@turma_id_empresas,@coligada_id_empresas
			END
		CLOSE ListaAtualizacaoEmpresas
		DEALLOCATE ListaAtualizacaoEmpresas

		----PROCURANDO AS FUNÇÕES QUE ESTÃO NESSA TURMA, PARA MIGRÁ-LAS PARA A NOVA TABELA
		DECLARE ListaAtualizacaoFuncoes CURSOR LOCAL STATIC FOR
			
			select  
			treinamentos_turmas_funcoes.curso_id,
			treinamentos_turmas_funcoes.turma_id,
			treinamentos_turmas_funcoes.funcao_id
			from treinamentos_turmas_funcoes (nolock)
			left join treinamentos_cursos (nolock) on treinamentos_cursos.id = treinamentos_turmas_funcoes.curso_id
			where treinamentos_cursos.ead = 1 and treinamentos_cursos.segregacao_id <> 3 and treinamentos_turmas_funcoes.turma_id = @id
			group by
			treinamentos_turmas_funcoes.curso_id,
			treinamentos_turmas_funcoes.turma_id,
			treinamentos_turmas_funcoes.funcao_id
			order by turma_id

		OPEN ListaAtualizacaoFuncoes
		FETCH NEXT FROM ListaAtualizacaoFuncoes INTO @curso_id_funcoes,@turma_id_funcoes,@funcao_id_funcoes

		WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO treinamentos_turmas_matriculados_funcoes
				(created_at,updated_at,updated_por,curso_id,turma_id,funcao_id,created_por)
				values
				(getdate(),getdate(),'ADMINISTRADOR INTERNO',@curso_id_funcoes,@novo_id_turma,@funcao_id_funcoes,'ADMINISTRADOR INTERNO')
				FETCH NEXT FROM ListaAtualizacaoFuncoes INTO @curso_id_funcoes,@turma_id_funcoes,@funcao_id_funcoes
			END
		CLOSE ListaAtualizacaoFuncoes
		DEALLOCATE ListaAtualizacaoFuncoes

		DECLARE ListaAtualizacaoParticipantes CURSOR LOCAL STATIC FOR
			
			select  
			treinamentos_participantes.curso_id,
			treinamentos_participantes.turma_id,
			treinamentos_participantes.matricula,
			treinamentos_participantes.situacao_id,
			treinamentos_participantes.situacao_data,
			treinamentos_participantes.nota,
			treinamentos_participantes.anexo
			from treinamentos_participantes (nolock)
			left join treinamentos_cursos (nolock) on treinamentos_cursos.id = treinamentos_participantes.curso_id
			where treinamentos_cursos.ead = 1 and treinamentos_cursos.segregacao_id <> 3 and treinamentos_participantes.turma_id = @id
			group by
			treinamentos_participantes.curso_id,
			treinamentos_participantes.turma_id,
			treinamentos_participantes.matricula,
			treinamentos_participantes.situacao_id,
			treinamentos_participantes.situacao_data,
			treinamentos_participantes.nota,
			treinamentos_participantes.anexo
			order by turma_id

		OPEN ListaAtualizacaoParticipantes
		FETCH NEXT FROM ListaAtualizacaoParticipantes INTO @curso_id_participantes,@turma_id_participantes,@matricula_participantes,@situacao_id_participantes,
		@situacao_data_participantes,@nota_participantes,@anexo_participantes

		WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO treinamentos_participantes_matriculados
				(created_at,updated_at,updated_por,curso_id,turma_id,matricula,situacao_id,situacao_data,nota,anexo,created_por)
				values
				(getdate(),getdate(),'ADMINISTRADOR INTERNO',@curso_id_participantes,@novo_id_turma,@matricula_participantes,@situacao_id_participantes,
				@situacao_data_participantes,@nota_participantes,@anexo_participantes,'ADMINISTRADOR INTERNO')

				FETCH NEXT FROM ListaAtualizacaoParticipantes INTO @curso_id_participantes,@turma_id_participantes,@matricula_participantes,@situacao_id_participantes,
				@situacao_data_participantes,@nota_participantes,@anexo_participantes
			END
		CLOSE ListaAtualizacaoParticipantes
		DEALLOCATE ListaAtualizacaoParticipantes

		-- Próxima linha
		FETCH NEXT FROM ListaTurmas INTO @id,@created_at,@updated_at,@updated_por,@curso_id,
@instrutor_id,@nome,@data,@obs,@anexo,@periodo_data1,@periodo_data2,@created_por,@conteudo
	END

CLOSE ListaTurmas
DEALLOCATE ListaTurmas
---------------------------------------------------------------------------------------------

END
-- EXEC proc_migra_treinamentos;