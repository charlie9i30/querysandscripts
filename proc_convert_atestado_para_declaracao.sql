USE [CorporeRMV12]
GO
/****** Object:  StoredProcedure [dbo].[proc_convert_atestado_para_declaracao]    Script Date: 1/9/2024 11:27:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec proc_convert_atestado_para_declaracao '2022-12-14', '2022-12-14' 
ALTER PROCEDURE [dbo].[proc_convert_atestado_para_declaracao](@PLN_Sheet1_$B2_D DATE,@PLN_Sheet1_$B3_D DATE) AS
BEGIN 
	DECLARE @coligada_id INT
	DECLARE @chapa varchar(250)
	DECLARE @funcionario varchar(250)
	DECLARE @codsituacao varchar(250)
	DECLARE @codsecao varchar(250)
	DECLARE @funcao varchar(250)
	DECLARE @secao varchar(250)
	DECLARE @data DATE
	DECLARE @motivo_ausencia varchar(250)
	DECLARE @escala_nome varchar(250)
	DECLARE @horario varchar(250)
	DECLARE @turno varchar(250)
	DECLARE @situacao varchar(250)
	DECLARE @supervisor varchar(250)
	DECLARE @data_motivo_id int
	

	DECLARE @COUNT int
	DECLARE @data_loop date
	DECLARE @data_inicio_aux date
	DECLARE @data_fim_aux date
	DECLARE @chapa_aux varchar(250)
	DECLARE @aux int
	DECLARE @motivo_ausencia_II varchar(250)
	
	DECLARE @coligada_id_l INT
	DECLARE @chapa_l varchar(250)
	DECLARE @funcionario_l varchar(250)
	DECLARE @datainicio_l DATE
	DECLARE @datafim_l DATE
	DECLARE @codabono varchar(250)
	DECLARE @abono varchar(250)
   
   SET @aux = 0
   
   IF OBJECT_ID('tempdb..#ListaPlanOcorrencia') is not null
	BEGIN
		TRUNCATE TABLE #ListaPlanOcorrencia	
	END
	ELSE
		BEGIN
			CREATE TABLE #ListaPlanOcorrencia (
				coligada_id INT,
				chapa varchar(250),
				funcionario varchar(250),
				codsituacao varchar(250),
				codsecao varchar(250),
				secao varchar(250),
				funcao varchar(250),
				supervisor varchar(250),
				data DATE,
				data_motivo_id int,
				motivo_ausencia varchar(250),
				escala_nome varchar(250),
				horario varchar(250),
				turno varchar(250),
			    situacao varchar(250)
			)
		END
	
   IF OBJECT_ID('tempdb..#ListaDeclaracoes') is not null
	BEGIN
		TRUNCATE TABLE #ListaDeclaracoes	
	END
	ELSE
		BEGIN
			CREATE TABLE #ListaDeclaracoes (
				coligada_id INT,
				chapa varchar(250),
				funcionario varchar(250),
				data date,
				codabono varchar(250),
				abono varchar(250)
			)
		END
	
   DECLARE ListaDeclaracoes CURSOR FOR 

			SELECT ZMDDECLARACOES.CODCOLIGADA,ZMDDECLARACOES.CHAPA,PFUNC.NOME,ZMDDECLARACOES.DATADOC,ZMDDECLARACOES.DATADOCFIM,ZMDDECLARACOES.CODABONO,ZMDABONOS.DESCRICAO
					FROM CorporeRMV12.dbo.ZMDDECLARACOES (NOLOCK) 
					INNER JOIN CorporeRMV12.dbo.ZMDABONOS (NOLOCK) ON  
					ZMDABONOS.CODCOLIGADA = ZMDDECLARACOES.CODCOLIGADA 
					AND ZMDABONOS.CODABONO = ZMDDECLARACOES.CODABONO 
					LEFT JOIN CorporeRMV12.dbo.PFUNC (NOLOCK) ON 
					PFUNC.CODCOLIGADA = ZMDDECLARACOES.CODCOLIGADA AND PFUNC.CHAPA = ZMDDECLARACOES.CHAPA
					WHERE 
					ZMDDECLARACOES.DATADOC BETWEEN  @PLN_Sheet1_$B2_D AND @PLN_Sheet1_$B3_D
  
  OPEN ListaDeclaracoes
  FETCH NEXT FROM ListaDeclaracoes INTO @coligada_id_l,@chapa_l,@funcionario_l,@datainicio_l,@datafim_l,@codabono,@abono
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @data_loop = @datainicio_l;

			WHILE DATEDIFF(DAY,@data_loop,@datafim_l) >= 0
				BEGIN
					INSERT INTO #ListaDeclaracoes(coligada_id,chapa,funcionario,data,codabono,abono)
					VALUES(@coligada_id_l,@chapa_l,@funcionario_l,@data_loop,@codabono,@abono)

					SET @data_loop = DATEADD(DAY,1,@data_loop)
				END		
			FETCH NEXT FROM ListaDeclaracoes INTO @coligada_id_l,@chapa_l,@funcionario_l,@datainicio_l,@datafim_l,@codabono,@abono
		END

	CLOSE ListaDeclaracoes
	DEALLOCATE ListaDeclaracoes
	
	
   DECLARE Lista CURSOR FOR 

			SELECT 
				empresas.coligada_id AS coligada_id, 
				planejamentos.matricula, 
				vwrmpfunc_all.nome AS matricula_nome,
				vwrmpfunc_all.codsituacao, 
				vwrmsecao.CODIGO as codsecao, 
				vwrmsecao.DESCRICAO AS secao, 
				codfuncao_nome as funcao, 
				supervisores.nome AS supervisor_nome,
				planejamentos.data,
				ISNULL(ocorrencias_motivos.id, 0) AS data_motivo_id,
				ISNULL(ocorrencias_motivos.nome, '---') AS data_motivo_nome,
				escalas.nome AS escala_nome,
				horarios.nome,
				turnos.nome as turno,
				CASE WHEN planejamentos.data_definicao = 1 THEN 'Trabalha' ELSE 'Folga' END AS situacao
				FROM [TelaWeb].dbo.planejamentos WITH (NOLOCK) 
				INNER JOIN [TelaWeb].dbo.alocacoes WITH (NOLOCK) ON alocacoes.id = planejamentos.alocacao_id AND alocacoes.matricula = planejamentos.matricula  
				INNER JOIN [TelaWeb].dbo.empresas WITH (NOLOCK) ON empresas.coligada_id = alocacoes.codcoligada 
				INNER JOIN [TelaWeb].dbo.vwrmpfunc_all 
				ON vwrmpfunc_all.codcoligada = alocacoes.codcoligada 
				AND vwrmpfunc_all.chapa COLLATE Latin1_General_CI_AI = alocacoes.matricula COLLATE Latin1_General_CI_AI 
				AND vwrmpfunc_all.chapa COLLATE Latin1_General_CI_AI = planejamentos.matricula COLLATE Latin1_General_CI_AI 
				INNER JOIN [TelaWeb].dbo.postos WITH (NOLOCK) ON postos.id = alocacoes.posto_id AND postos.codcoligada = alocacoes.codcoligada 
				INNER JOIN [TelaWeb].dbo.vwrmsecao ON vwrmsecao.CODIGO COLLATE Latin1_General_CI_AI = postos.secao_id COLLATE Latin1_General_CI_AI AND vwrmsecao.CODCOLIGADA = postos.codcoligada 
				INNER JOIN [TelaWeb].dbo.escalas WITH (NOLOCK) ON escalas.id = alocacoes.escala_id AND escalas.id = planejamentos.escala_id 
				INNER JOIN [TelaWeb].dbo.horarios WITH (NOLOCK) ON horarios.id = alocacoes.horario_id 
				INNER JOIN [TelaWeb].dbo.horariosds WITH (NOLOCK) ON horariosds.horario_id = horarios.id AND horariosds.ds = planejamentos.ds 
				INNER JOIN [TelaWeb].dbo.turnos WITH (NOLOCK) ON turnos.id = alocacoes.turno_id 
				LEFT OUTER JOIN [TelaWeb].dbo.ocorrencias_motivos WITH (NOLOCK) ON ocorrencias_motivos.id = planejamentos.data_motivo
				LEFT JOIN [TelaWeb].dbo.supervisores WITH (NOLOCK) ON supervisores.id = postos.supervisor_id
				where [TelaWeb].dbo.planejamentos.data BETWEEN @PLN_Sheet1_$B2_D and @PLN_Sheet1_$B3_D 
				and (planejamentos.matricula NOT LIKE 'D%') AND (planejamentos.matricula NOT LIKE 'P%') AND
				ocorrencias_motivos.id NOT IN(0,4,20,22) 
			
			
	OPEN Lista	
	FETCH NEXT FROM Lista INTO @coligada_id,@chapa,@funcionario,@codsituacao,@codsecao,@secao,@funcao,@supervisor,@data,@data_motivo_id,@motivo_ausencia,@escala_nome,@horario,@turno,@situacao
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @COUNT = COUNT(*) 
			FROM #ListaDeclaracoes (NOLOCK) 
			WHERE coligada_id = @coligada_id AND chapa = @chapa
			AND data = @data
			
			IF(@COUNT > 0 )
				BEGIN
					SELECT TOP 1 @motivo_ausencia_II =  abono FROM #ListaDeclaracoes (NOLOCK) WHERE coligada_id = @coligada_id AND chapa = @chapa AND data = @data
				END
			IF(@COUNT = 0)
				BEGIN
					SET @motivo_ausencia_II = @motivo_ausencia
				END
				
			
				insert into #ListaPlanOcorrencia 
					(coligada_id,chapa,funcionario,codsituacao,codsecao,secao,data,motivo_ausencia,escala_nome,horario,turno,situacao) 
				values 
					(@coligada_id,@chapa,@funcionario,@codsituacao,@codsecao,@secao,@data,@motivo_ausencia_II,@escala_nome,@horario,@turno,@situacao)
	
				FETCH NEXT FROM Lista INTO @coligada_id,@chapa,@funcionario,@codsituacao,@codsecao,@secao,@funcao,@supervisor,@data,@data_motivo_id,@motivo_ausencia,@escala_nome,@horario,@turno,@situacao
		END
	CLOSE Lista
	DEALLOCATE Lista	

	SELECT coligada_id,chapa,funcionario,codsituacao,codsecao,secao,data,motivo_ausencia,escala_nome,horario,turno,situacao FROM #ListaPlanOcorrencia ORDER BY coligada_id,chapa,data
end
