USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_etiqueta_folha_ponto_automatiza]    Script Date: 1/9/2024 11:17:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC proc_etiqueta_folha_ponto_automatiza
ALTER PROCEDURE [dbo].[proc_etiqueta_folha_ponto_automatiza] AS

DECLARE @data_ini VARCHAR(22)
DECLARE @data_fim VARCHAR(22)
declare @ano_full INT
declare @mes_full VARCHAR(2)

DECLARE @supervisores_id INT
DECLARE @supervisores_nome VARCHAR(255)
DECLARE @supervisores_postos INT

DECLARE @arquivo VARCHAR(MAX) = ''
DECLARE @processo VARCHAR(MAX) = ''
DECLARE @ID_PROC INT 

SET @ID_PROC = 55
		
--Zerar Total Concluído
UPDATE [DW].dbo.processos_dw 
SET total_meta= 1,total_pendente= 0
WHERE ID=@ID_PROC;

-------------------------------------------------------------------------------------------------------------------
INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
								VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
-------------------------------------------------------------------------------------------------------------------

-- AGENDAS POR COLIGADAS: 1 DIA 20 / 6 DIA 21 / 2 DIA 22 / 5 DIA 23

-- Coligada 1
-- Coligada 1
-- Coligada 1
print('---')
IF DAY(GETDATE()) = 20 AND (DATEPART(HOUR, GETDATE()) >= 0 OR DATEPART(HOUR, GETDATE()) <= 7)
	BEGIN
		print('Monta a Competência e Período')
		SET @ano_full = YEAR(GETDATE())
		SET @mes_full = MONTH(DATEADD(MONTH,1,GETDATE()))
		-- Tratamento para mês 12 colocar o Ano + 1
		IF MONTH(GETDATE()) = '12' AND @mes_full = 1 
			BEGIN 
				SET @ano_full = @ano_full + 1
			END
		SET @data_ini = CONCAT(CONVERT(VARCHAR(4),@ano_full),'-',CONVERT(VARCHAR(4),MONTH(DATEADD(MONTH,1,GETDATE()))),'-01')
		SET @data_fim = EOMONTH(CONVERT(DATE,@data_ini))
		print(@data_ini + '-A-' + @data_fim)

		print('---')
		print('*** Coligada 1 - Geração da folha de Etiquetas - frequencia_id = 4') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 1 and frequencia_id = 4
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('ET-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-etiquetas-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=1'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers

		print('---')
		print('*** Coligada 1 - Geração da folha de Ponto - frequencia_id = 2') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 1 and frequencia_id = 2
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('FP-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-folhasponto-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=1'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
							
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers
	END 

-- Coligada 6
-- Coligada 6
-- Coligada 6
print('---')
IF DAY(GETDATE()) = 21 AND (DATEPART(HOUR, GETDATE()) >= 0 OR DATEPART(HOUR, GETDATE()) <= 7)
	BEGIN
		print('Monta a Competência e Período')
		SET @ano_full = YEAR(GETDATE())
		SET @mes_full = MONTH(DATEADD(MONTH,1,GETDATE()))
		-- Tratamento para mês 12 colocar o Ano + 1
		IF MONTH(GETDATE()) = '12' AND @mes_full = 1
			BEGIN 
				SET @ano_full = @ano_full + 1
			END
		SET @data_ini = CONCAT(CONVERT(VARCHAR(4),@ano_full),'-',CONVERT(VARCHAR(4),MONTH(DATEADD(MONTH,1,GETDATE()))),'-01')
		SET @data_fim = EOMONTH(CONVERT(DATE,@data_ini))
		print(@data_ini + '-A-' + @data_fim)

		print('---')
		print('*** Coligada 6 - Geração da folha de Etiquetas - frequencia_id = 4') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 6 and frequencia_id = 4
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('ET-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-etiquetas-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=6'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
													
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers

		print('---')
		print('*** Coligada 6 - Geração da folha de Ponto - frequencia_id = 2') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 6 and frequencia_id = 2
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('FP-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-folhasponto-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=6'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
														
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers
	END 

-- Coligada 2
-- Coligada 2
-- Coligada 2
print('---')
IF DAY(GETDATE()) = 22 AND (DATEPART(HOUR, GETDATE()) >= 0 OR DATEPART(HOUR, GETDATE()) <= 7)
	BEGIN
		print('Monta a Competência e Período')
		SET @ano_full = YEAR(GETDATE())
		SET @mes_full = MONTH(DATEADD(MONTH,1,GETDATE()))
		-- Tratamento para mês 12 colocar o Ano + 1
		IF MONTH(GETDATE()) = '12' AND @mes_full = 1
			BEGIN 
				SET @ano_full = @ano_full + 1
			END
		SET @data_ini = CONCAT(CONVERT(VARCHAR(4),@ano_full),'-',CONVERT(VARCHAR(4),MONTH(DATEADD(MONTH,1,GETDATE()))),'-01')
		SET @data_fim = EOMONTH(CONVERT(DATE,@data_ini))
		print(@data_ini + '-A-' + @data_fim)

		print('---')
		print('*** Coligada 2 - Geração da folha de Etiquetas - frequencia_id = 4') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 2 and frequencia_id = 4
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('ET-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-etiquetas-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=2'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
																			
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers

		print('---')
		print('*** Coligada 2 - Geração da folha de Ponto - frequencia_id = 2') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 2 and frequencia_id = 2
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('FP-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-folhasponto-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=2'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
														
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers
	END 
	
-- Coligada 5
-- Coligada 5
-- Coligada 5
print('---')
IF DAY(GETDATE()) = 23 AND (DATEPART(HOUR, GETDATE()) >= 0 OR DATEPART(HOUR, GETDATE()) <= 7)
	BEGIN
		print('Monta a Competência e Período')
		SET @ano_full = YEAR(GETDATE())
		SET @mes_full = MONTH(DATEADD(MONTH,1,GETDATE()))
		-- Tratamento para mês 12 colocar o Ano + 1
		IF MONTH(GETDATE()) = '12' AND @mes_full = 1
			BEGIN 
				SET @ano_full = @ano_full + 1
			END
		SET @data_ini = CONCAT(CONVERT(VARCHAR(4),@ano_full),'-',CONVERT(VARCHAR(4),MONTH(DATEADD(MONTH,1,GETDATE()))),'-01')
		SET @data_fim = EOMONTH(CONVERT(DATE,@data_ini))
		print(@data_ini + '-A-' + @data_fim)

		print('---')
		print('*** Coligada 5 - Geração da folha de Etiquetas - frequencia_id = 4') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 5 and frequencia_id = 4
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('ET-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-etiquetas-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=5'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
														
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers

		print('---')
		print('*** Coligada 5 - Geração da folha de Ponto - frequencia_id = 2') 
		print('---')
		DECLARE ListaSupers CURSOR LOCAL STATIC FOR
			select vwsupervisores.id, vwsupervisores.nome, count(vwpostos.id) AS qtd
			from vwsupervisores WITH (NOLOCK) 
			JOIN 
			( 
			select * from vwpostos with (nolock) 
			where ativo = 1 and coligada_id = 5 and frequencia_id = 2
			) vwpostos ON vwpostos.supervisor_id = vwsupervisores.id 
			where vwsupervisores.ativo = 1 
			GROUP BY vwsupervisores.id, vwsupervisores.nome
			order by nome

			OPEN ListaSupers
			FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos

			WHILE @@FETCH_STATUS = 0
				BEGIN
					print('---')
					print('Avalia Supervisor [' + @supervisores_nome + ']')
					print('Verifica se já tem arquivo gerado para esse Período') 
					set @arquivo = 
					(
					SELECT TOP 1 arquivo
					FROM impressoes_process WITH (NOLOCK) 
					WHERE arquivo like CONCAT('FP-%SUPERVISOR-',REPLACE(@supervisores_nome,' ','-'),'%',@data_ini,'-A-',@data_fim,'%')
					ORDER BY id DESC
					)

					IF @arquivo IS NULL OR LEN(@arquivo) < 1
						BEGIN 
							SET @processo = 'https://intranet.gruposerval.com.br/tela/alocacao/impressoes-folhasponto-process.php'
							SET @processo = @processo + '?updated_por=''Adminstrador Interno'''
							SET @processo = @processo + '&codcoligada=5'
							SET @processo = @processo + '&supervisor_id=' + CONVERT(VARCHAR(11),@supervisores_id)
							SET @processo = @processo + '&cliente_id='
							SET @processo = @processo + '&posto_id='
							SET @processo = @processo + '&matricula='
							SET @processo = @processo + '&data1=' + FORMAT(CONVERT(DATE,@data_ini),'dd/MM/yyyy')
							SET @processo = @processo + '&data2=' + FORMAT(CONVERT(DATE,@data_fim),'dd/MM/yyyy')
							print(@processo)
														
							insert into usuariosprocessos 
							(created_at,updated_at,created_por,updated_por,usuario_id,processo,total,atual,ativo,matricula,coligada_id) 
							values 
							(getdate(),getdate(),'Adminstrador Interno','Adminstrador Interno',1,@processo,1,1,1,NULL,1) 
							

							print('Enviado para fila do Sonar')
						END 
					ELSE 
						BEGIN 
							print('Já Processado')
						END

					FETCH NEXT FROM ListaSupers INTO @supervisores_id,@supervisores_nome,@supervisores_postos
				END 

		CLOSE ListaSupers
		DEALLOCATE ListaSupers
	END 

UPDATE [DW].dbo.processos_dw 
SET data_exec= GETDATE(),[STATUS]=1,total_meta= 0,total_pendente= 0
WHERE ID=@ID_PROC;
		
-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE(), registros_total = 0 where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
-------------------------------------------------------------------------------------------------------------------



print('Operação Concluida ')