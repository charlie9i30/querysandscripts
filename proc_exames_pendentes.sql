USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_exames_pendentes]    Script Date: 1/9/2024 11:17:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[proc_exames_pendentes] AS
BEGIN
	SET NOCOUNT ON;
DECLARE @ID_PROC BIGINT
DECLARE @COUNT  BIGINT
-- Variáveis do atestado
DECLARE @codcoligada INT	
DECLARE @chapa varchar(22)
DECLARE @nome varchar(255)
DECLARE @funcao varchar(255)
DECLARE @secao varchar(255)
DECLARE @clinica varchar(255)
DECLARE @emailFuncionario varchar(255)
DECLARE @emailClinica varchar(255)
DECLARE @emailOpcional varchar(255)
DECLARE @emailSupervisor varchar(255)
DECLARE @emailSuperior varchar(255)
DECLARE @supervisor varchar(255)
DECLARE @codsituacao varchar(255)
DECLARE @situacao varchar(255)
DECLARE @admissao varchar(22)
DECLARE @exame_ultimo varchar(22)
DECLARE @exame_proximo varchar(22)
DECLARE @exame_status varchar(255)
DECLARE @exame_cod varchar(255)
DECLARE @exame_nome varchar(255)
DECLARE @exame_tipo varchar(255)
DECLARE @horario varchar(255)


SET @ID_PROC=32


BEGIN
	-------------------------------------------------------------------------------------------------------------------
	UPDATE [DW].dbo.processos_dw SET total_meta= 1,total_pendente= 0 WHERE ID=@ID_PROC;
	-------------------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------------------
	INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
									VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
	-------------------------------------------------------------------------------------------------------------------

	print '----------------------------------------------------------------------------'
	print '-- Atualização da tabela de controle de exames...'
	print '----------------------------------------------------------------------------'
	--EXEC proc_exames_pendentes


	-- Limpa dados atuais
	print '-- Limpa dados atuais...'
	TRUNCATE TABLE rmpfunc_exames;

	INSERT INTO rmpfunc_exames 
		(created_at, created_por, updated_at, updated_por, 
			codcoligada, chapa, nome, funcao, secao, clinica, supervisor,codsituacao, 
			situacao, admissao, exame_ultimo, exame_proximo, 
			exame_status,exame_cod,exame_nome, exame_tipo, horario,email_funcionario,email_clinica,email_opcional,email_supervisor,email_superior) 
	SELECT getdate() created_at, 'ADM' created_por, getdate() updated_at, 'ADM' updated_por, 
		CodColigada,chapa,[Funcionário],[Função],[Seção],Clinica,Supervisor,
		[Cód.Situação],[Situação],[Data de Admissão],[Data do Último Exame],[Data Próx.Exame],Status,[Cód.Exame],Exame,
		Exame_Tipo,[Horário],EmailFuncionario,EmailClinica,EmailOpcional,EmailSupervisor,EmailSuperior
		from [CorporeRMV12].[dbo].[VW_Exames_Pendentes_Cubo] (nolock)

	-------------------------------------------------------------------------------------------------------------------
	UPDATE [DW].dbo.processos_dw SET data_exec = getdate(),[STATUS]=1,total_meta = 0,total_pendente = 0 WHERE ID=@ID_PROC;
	-------------------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------------------
	UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE(), registros_total = @COUNT where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
	-------------------------------------------------------------------------------------------------------------------

	print('Processamento encerrado com sucesso');

END

END

