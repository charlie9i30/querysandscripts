USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_migra_folhadeponto]    Script Date: 1/9/2024 11:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --EXEC proc_migra_folhadeponto;
ALTER PROCEDURE [dbo].[proc_migra_folhadeponto] as 
begin
	declare @ID int
	declare @criado_at datetime
	declare @atualizado_at datetime
	declare @atualizado_por varchar(250)
	declare @ID_Campo int
	declare @Resposta varchar(250)

	declare @Empresa varchar(250)
	declare @chapa varchar(250)
	declare @codcoligada varchar(250)
	declare @EmpresaII varchar(250)
	declare @CodCliente varchar(250)
	declare @CodLotacao varchar(250)
	declare @Funcionario varchar(250)
	declare @Funcionario_II varchar(250)
	declare @matricula varchar(250)
	declare @CodLocalidade varchar(250)
	declare @Lotacao varchar(250)
	declare @Lotacao_nome varchar(250)
	declare @coligada_nome varchar(250)
	declare @NomeLocalidade_II varchar(250)
	declare @Periodo_II varchar(250)
	declare @Supervisor_II varchar(250)
	declare @Periodo varchar(250)
	declare @Posto varchar(250)
	declare @Supervisor varchar(250)
	declare @Supervisor_id varchar(250)
	declare @Supervisor_nome varchar(250)
	declare @Funcionario_nome varchar(250)
	declare @RetornoPasta varchar(250)
	declare @horario_britanico varchar(250) 
	declare	@origem varchar(250)
	declare	@horario_incompleto varchar(250)
	declare	@rasura varchar(250)
	declare	@hora_escala varchar(250)
	declare	@intrajornada varchar(250)
	declare @assinatura_funcionario varchar(250)
	declare @assinatura_supervisor varchar(250)
	declare @Ano varchar(250)
	declare @Mes varchar(250)
	declare @Dia varchar(250)
	declare @Aux varchar(250)
	declare @AuxII varchar(250)
	declare @AuxIII varchar(250)
	
	declare @AnoII varchar(250)
	declare @MesII varchar(250)
	declare @DiaII varchar(250)
	
	declare @OCOR int
	declare @qrcode varchar(250)

	declare @ID_PROC bigint
	declare @COUNT bigint
   
	declare @SQL varchar(MAX)

	SET @COUNT = 0
	SET @ID_PROC = 58

	IF OBJECT_ID('tempdb..#folha_de_ponto') is not NULL
		BEGIN 	
			-- Se já existe exclui
			TRUNCATE TABLE #folha_de_ponto
		END
	ELSE
		BEGIN
			-- Cria novamente
			CREATE TABLE #folha_de_ponto(
				id int,
				criado_at datetime, 
				atualizado_at datetime,
				mes varchar(250),
				ano varchar(250),
				codcoligada varchar(250),
				empresa varchar(250),
				supervisor_id varchar(250),
				supervisor varchar(250),
				periodo varchar(250),
				chapa varchar(250),
				funcionario varchar(250),
				posto_id varchar(250),
				posto varchar(250),
				retornopasta varchar(250),
				horario_britanico varchar(250),
				hora_escala varchar(250),
				intrajornada varchar(250),
				origem varchar(250),
				data1 date, 
				data2 date,
				atualizado_por varchar(250),
				rasura varchar(250),
				horario_incompleto varchar(250),
				assinatura_funcionario varchar(250),
				assinatura_supervisor varchar(250)
				)
		END

		-------------------------------------------------------------------------------------------------------------------
		INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
										VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
		-------------------------------------------------------------------------------------------------------------------
		----------------------------------------------------------------------------
		--Zerar Total Concluído
		UPDATE [DW].dbo.processos_dw SET total_meta= 1,total_pendente = 0 WHERE ID = @ID_PROC;
		----------------------------------------------------------------------------


	declare Lista45 cursor Local Static for	
		SELECT registro_id,created_at,updated_at,updated_por,campo_id,resposta_texto 
		from forms_registros_respostas (nolock)
		where form_id = 45 AND campo_id in (404,405,406,407,408,421,456,411,412,413,414,415,416,418)
		order by registro_id desc,campo_id asc

		--SELECT ID,Registro_Criado_At,Registro_Atualizado_At,ID_Campo,Resposta 
		--from vwforms_registros_respostas 
		--where ID_Formulario = 45 AND ID_Campo in (404,405,406,407,408,421)
		--order by ID desc,ID_Campo asc


	OPEN Lista45
	FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
	SET @COUNT = @@CURSOR_ROWS;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- 404 Empresa
			-- 405 Supervisor
			-- 406 Funcionário
			-- 407 Lotação
			-- 408 Período
			-- 411 Horario Britânico
			-- 412 Horário Incompleto
			-- 413 Ass.: Funcionário
			-- 414 Ass.: Supervisor
			-- 415 Rasura
			-- 416 Hora/Escala
			-- 418 Intrajornada
			-- 421 RetornoPasta	
			-- 456 Origem
			
			IF (@ID_Campo = 404) 
				BEGIN
					SET @Empresa = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 405) 
				BEGIN
					SET @Supervisor = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 406) 
				BEGIN
					SET @Funcionario = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 407) 
				BEGIN
					SET @Lotacao = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 408) 
				BEGIN
					SET @Periodo = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 411) 
				BEGIN
					SET @horario_britanico = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 412) 
				BEGIN
					SET @horario_incompleto = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 413) 
				BEGIN
					SET @assinatura_funcionario = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 414) 
				BEGIN
					SET @assinatura_supervisor = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 415) 
				BEGIN
					SET @rasura = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 416) 
				BEGIN
					SET @hora_escala = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 418) 
				BEGIN
					SET @intrajornada = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 421) 
				BEGIN
					SET @RetornoPasta = @Resposta
					FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
				END
			IF (@ID_Campo = 456) 
				BEGIN
					SET @origem = @Resposta
				END
			
			SET @codcoligada = LEFT(@Empresa,1)
			SET @coligada_nome = RTRIM(LTRIM(SUBSTRING(@Empresa,5,len(@Empresa))))
			--(01 - Cliente - Localidade) Retirando apenas o Código do Posto e buscando o código do cliente
			SET @CodLotacao = dbo.GetNumeric(SUBSTRING(@Lotacao,(LEN(@Lotacao) - (CHARINDEX('(',REVERSE(@Lotacao))-1)),CHARINDEX('(',REVERSE(@Lotacao))))
			SELECT @CodCliente = cliente_id FROM [TelaWeb].[dbo].[vwpostos] (NOLOCK) WHERE id = @CodLotacao
			--(02 - Funcionario - Matricula) Retirando apenas a Matricula do Funcionario
			SET @chapa = Right(replicate('0000000',7) + convert(VARCHAR,dbo.GetNumeric(RIGHT(@Funcionario,7))),7) 
			SET @Supervisor_id = convert(VARCHAR,dbo.GetNumeric(RIGHT(@Supervisor,7))) 
			--(03 - Funcionario - Nome) Retirando apenas o nome do funcionario 
			SET @Funcionario_nome = SUBSTRING(RTRIM(LTRIM(@Funcionario)),1,CHARINDEX(' (',@Funcionario)-1)
			if CHARINDEX(' (',@Lotacao)-1 > 0  
				SET @Lotacao_nome = SUBSTRING(RTRIM(LTRIM(@Lotacao)),1,CHARINDEX(' (',@Lotacao)-1)
			else 
				SET @Lotacao_nome = @Lotacao
			
			SET @Supervisor_nome = SUBSTRING(RTRIM(LTRIM(@Supervisor)),1,CHARINDEX(' (',@Supervisor)-1)
			--(04 - Periodo)
			SET @Dia = SUBSTRING(@Periodo,1,2)
			SET @Mes = SUBSTRING(@Periodo,4,2)
			SET @Ano = SUBSTRING(@Periodo,7,4)
			SET @DiaII = SUBSTRING(@Periodo,14,2)
			SET @MesII = SUBSTRING(@Periodo,17,2)
			SET @AnoII = SUBSTRING(@Periodo,20,4)
			IF (@Ano = '2022') 
				SET @AuxIII = @AuxIII + '-' + @Ano + '/' +@Mes
			ELSE 
				SET @AuxIII = @AuxIII + '-' + @AnoII + '/' +@MesII + 'de' + @Dia + 'a' + @DiaII

			/*SET @Periodo = @Ano + '-' + @Mes + '-' + @Dia + 'a' + @AnoII + '-' + @MesII + '-' + @DiaII*/

			

--select * from fc_forms_registros_respostas_45()   order by id desc where ano like '%-%'
			insert into #folha_de_ponto 
				(id,criado_at,atualizado_at,mes,ano,
				codcoligada,empresa,supervisor_id,supervisor,periodo,
				chapa,funcionario,posto_id,posto,retornopasta,
				horario_britanico,hora_escala,intrajornada,origem,data1,data2,
				atualizado_por,rasura,horario_incompleto,assinatura_funcionario,assinatura_supervisor)
			values 
				(@id,@criado_at,@atualizado_at,CASE WHEN @Ano='2023' AND @MesII>'01' THEN @MesII ELSE @Mes END, --@Mes, 
				CASE WHEN @Ano='2023' AND @MesII>'01' THEN @AnoII ELSE @Ano END,--@Ano,
				@codcoligada,@coligada_nome,@Supervisor_id,@Supervisor_nome,@Periodo,@chapa,@Funcionario_nome,@CodLotacao,
				@Lotacao_nome,@RetornoPasta,@horario_britanico,@hora_escala,@intrajornada,@origem,
				@Ano +'-'+ @Mes +'-'+ @Dia,@AnoII +'-'+ @MesII +'-'+ @DiaII,@atualizado_por,
				@rasura,@horario_incompleto,@assinatura_funcionario,@assinatura_supervisor)


			FETCH NEXT FROM Lista45 INTO @ID,@criado_at,@atualizado_at,@atualizado_por,@ID_Campo,@Resposta 
		END
	CLOSE Lista45
	DEALLOCATE Lista45
	
	TRUNCATE TABLE folha_de_ponto_bd
	
	INSERT INTO folha_de_ponto_bd 
	select * from #folha_de_ponto

	TRUNCATE TABLE #folha_de_ponto

	UPDATE [DW].dbo.processos_dw SET data_exec= GETDATE(),[STATUS]=1,total_meta= 0,total_pendente= 0 WHERE ID = @ID_PROC;

	-------------------------------------------------------------------------------------------------------------------
	UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE(), registros_total = @COUNT where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
	-------------------------------------------------------------------------------------------------------------------

end
--mes	ano	supervisor	periodo
--2-	0-11	JONATHAN MESQUITA (96)	0-11-2--20a10-11-22