USE [TelaWeb]
GO
/****** Object:  StoredProcedure [dbo].[proc_criar_protocolos_from_forms]    Script Date: 1/9/2024 11:17:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC proc_criar_protocolos_from_forms
ALTER PROCEDURE [dbo].[proc_criar_protocolos_from_forms] as
BEGIN
	declare @form_id int
	declare @form_nome varchar(255)
	declare @form_id_hash varchar(255)
	declare @tipo_id int
	declare @created_por varchar(255)
	declare @updated_at datetime
	declare @registro_id int
	declare @pessoa_id int
	declare @departamento_id int
	declare @protocolo varchar(255)
	declare @descricao varchar(255)
	declare @resposta_texto varchar(255)
	declare @resposta varchar(255)
	declare @atendimento_id int
	
	DECLARE @ID_PROC BIGINT;
	SET @ID_PROC = 49;

	UPDATE [DW].dbo.processos_dw SET total_meta= 1,total_pendente= 0 WHERE ID=@ID_PROC;

	-------------------------------------------------------------------------------------------------------------------
	INSERT INTO [DW].dbo.processos_dw_hist(created_por,created_at,updated_at,updated_por,id_processo,data_exec_ini)
									VALUES('ADM',getdate(),getdate(),'ADM',@ID_PROC,getdate())
	---------------------------------------------------------------------------------------------------------------

	DECLARE ListaCriarP Cursor Local Static For
		select 
		T.*, 
		CONCAT(
		T.resposta_texto,
		' # <a href="../public/form_view.php?id=',
		CONVERT(varchar,T.registro_id),
		CONVERT(varchar,T.form_id_hash),
		'" class="badge badge-dark" target="_blank">',
		T.form_nome,
		'</a>'
		) AS resposta
		from (
			SELECT forms_cadastro.id AS form_id, 
			forms_cadastro.nome as form_nome,
			CASE 
			WHEN forms_cadastro.id = 10 THEN '&form_id=1ds='
			WHEN forms_cadastro.id = 18 THEN '&form_id=1dM='
			WHEN forms_cadastro.id = 38 THEN '&form_id=19M='
			WHEN forms_cadastro.id = 46 THEN '&form_id=0N0='
			WHEN forms_cadastro.id = 66 THEN '&form_id=0t0='
			ELSE '00000'
			END AS form_id_hash,
			forms_cadastro.criar_protocolo_tipo_id AS tipo_id,
			forms_registros.created_por, forms_registros.updated_at, 
			forms_registros.id AS registro_id,
			forms_registros.pessoa_id,
			forms_cadastro.departamento_id,
			FORMAT(forms_registros.updated_at,'yyyyMMddHHmmss') AS protocolo,
			concat(forms_registros_respostas.resposta_texto,' # ',forms_cadastro.nome) AS descricao,
			forms_registros_respostas.resposta_texto
			FROM forms_cadastro WITH (nolock) 
			JOIN forms_registros WITH (nolock) ON forms_registros.form_id=forms_cadastro.id  
			LEFT JOIN forms_registros_respostas WITH (nolock) ON 
			forms_registros_respostas.registro_id = forms_registros.id 
			AND forms_registros_respostas.campo_id = forms_cadastro.criar_protocolo_campo_id
			WHERE forms_cadastro.criar_protocolo_concluir = 1 
			AND forms_cadastro.criar_operacao_concluir <= 0 
			AND (forms_registros.atendimento_id IS NULL OR forms_registros.atendimento_id = 0)
		) T
		ORDER BY T.form_id, T.registro_id

	Open ListaCriarP
	Fetch Next From ListaCriarP Into 
	@form_id,@form_nome,@form_id_hash,@tipo_id,@created_por,@updated_at,@registro_id,@pessoa_id,@departamento_id,@protocolo,@descricao,@resposta_texto,@resposta

	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Insert na tabela de registros 
			insert into atendimentos_registros
            (created_at,updated_at,created_por,updated_por,tipo_id,sub_tipo_id,prioridade_id,protocolo,pessoa_id,
			 status_id,descricao,avaliacao_id,agendado_data,preferencial,ativo,arquivo_fisico)
			values 
			(@updated_at,@updated_at,@created_por,@created_por,@tipo_id,0,1,@protocolo,@pessoa_id,
			 0,@descricao,1,NULL,0,1,0) 

			-- Insert na tabela de fluxo
			set @atendimento_id = (select max(id) from atendimentos_registros with (nolock) where tipo_id = @tipo_id and protocolo = @protocolo)
			insert into atendimentos_fluxo 
            (created_at,updated_at,created_por,updated_por,atendimento_id,departamento_id,responsavel_id,status_id,descricao,resposta,lido,anexo_url) 
            values 
            (@updated_at,@updated_at,@created_por,@created_por,@atendimento_id,@departamento_id,0,0,@descricao,@resposta,0,NULL) 

			-- update no form_registros para ligar as coisas
			update forms_registros set atendimento_id = @atendimento_id where id  = @registro_id

			Fetch Next From ListaCriarP Into 
			@form_id,@form_nome,@form_id_hash,@tipo_id,@created_por,@updated_at,@registro_id,@pessoa_id,@departamento_id,@protocolo,@descricao,@resposta_texto,@resposta
		END
	CLOSE ListaCriarP
	DEALLOCATE ListaCriarP

UPDATE [DW].dbo.processos_dw SET data_exec=getdate(),[STATUS]=1,total_meta= 0,total_pendente= 0 WHERE ID=@ID_PROC;

-------------------------------------------------------------------------------------------------------------------
UPDATE [DW].dbo.processos_dw_hist SET data_exec_fim = GETDATE() where id = (SELECT MAX(id) as id FROM [DW].dbo.processos_dw_hist (NOLOCK) WHERE id_processo = @ID_PROC)
------------


END